/**
 * 
 * Questao sobre lista estatica desordenada.
 *
 */
public class Questao2 {

	Pessoa[] infos;
	int qtdePessoas = 0;
	
	void inicializar(int tamanho) {
		infos = new Pessoa[tamanho];
	}
	
	boolean inserir(Pessoa p) {
		if (qtdePessoas >= infos.length) {
			return false;
		}
		
		infos[qtdePessoas++] = p;
		
		return true;
	}
	
	/**
	 * Valor da questao: 5,0
	 * 
	 * Considerando a lista estatica desordenada que armazena registros 
	 * do tipo Pessoa, retorne a quantidade de pessoas da lista que tenham 
	 * idade maior que a idade de corte passada como parametro de entrada.
	 * 
	 * Exemplo 1:
	 * infos = {Joao|25, Maria|10, Ruy|50, Pedro|66}
	 * idadeCorte = 20
	 * retorno = 3
	 * 
	 * @param idadeCorte
	 * @return
	 * for (int i = 0; i < infos.length; i++) {
			if(infos[i].idade > idadeCorte);
			qtdePessoas = infos[i].idade;
		}
		return qtdePessoas;
	 * 
	 */
	int contarMaisVelhos(int idadeCorte) {
		int qtdeMaisVelhos = 0;
		for (int i = 0; i < qtdePessoas; i++) {
			if(infos[i].idade > idadeCorte){
			qtdeMaisVelhos++;
			}
		}
		return qtdeMaisVelhos;
	}
}
