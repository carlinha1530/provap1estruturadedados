/**
 * 
 * Testes para a questao 1.
 * 
 * NAO ALTERE ESTE ARQUIVO.
 *
 */
public class Questao1Testes {

	public static void todosTestes() {
		
		boolean teste01 = teste01();
		
		boolean teste02 = teste02();
		
		boolean teste03 = teste03();
		
		boolean teste04 = teste04();
		
		boolean teste05 = teste05();
		
		boolean teste06 = teste06();
		
		boolean teste07 = teste07();
		
		boolean teste08 = teste08();
		
		boolean teste09 = teste09();
				
		if (
				teste01 &
				teste02 &
				teste03 &
				teste04 & 
				teste05 &
				teste06 & 
				teste07 &
				teste08 & 
				teste09 &
				true) {
			System.out.println("Todos os testes para a Questao 1 realizados com sucesso!");
		}
	}

	private static boolean teste01() {
		Questao1 questao = new Questao1();
		
		int[] resultado = questao.calcularSomaProduto(new int[]{ 1, 2, 3 });
		
		if (resultado == null) {
			System.err.println("Erro Questao1 - teste01 - 1");
			return false;
		}

		if (resultado.length != 2) {
			System.err.println("Erro Questao1 - teste01 - 2");
			return false;
		}

		if (resultado[0] != 6) {
			System.err.println("Erro Questao1 - teste01 - 3");
			return false;
		}

		if (resultado[1] != 6) {
			System.err.println("Erro Questao1 - teste01 - 4");
			return false;
		}

		return true;
	}
	
	
	
	private static boolean teste02() {
		Questao1 questao = new Questao1();
		
		int[] resultado = questao.calcularSomaProduto(new int[]{ 1, 1, 1 });
		
		if (resultado == null) {
			System.err.println("Erro Questao1 - teste02 - 1");
			return false;
		}

		if (resultado.length != 2) {
			System.err.println("Erro Questao1 - teste02 - 2");
			return false;
		}

		if (resultado[0] != 3) {
			System.err.println("Erro Questao1 - teste02 - 3");
			return false;
		}

		if (resultado[1] != 1) {
			System.err.println("Erro Questao1 - teste02 - 4");
			return false;
		}

		return true;
	}
	
	
	private static boolean teste03() {
		Questao1 questao = new Questao1();
		
		int[] resultado = questao.calcularSomaProduto(new int[]{ 2, 3, 4 });
		
		if (resultado == null) {
			System.err.println("Erro Questao1 - teste03 - 1");
			return false;
		}

		if (resultado.length != 2) {
			System.err.println("Erro Questao1 - teste03 - 2");
			return false;
		}

		if (resultado[0] != 9) {
			System.err.println("Erro Questao1 - teste03 - 3");
			return false;
		}

		if (resultado[1] != 24) {
			System.err.println("Erro Questao1 - teste03 - 4");
			return false;
		}

		return true;
	}
	
	
	
	private static boolean teste04() {
		Questao1 questao = new Questao1();
		
		int[] resultado = questao.calcularSomaProduto(new int[]{ 7, 3, 5, 2 });
		
		if (resultado == null) {
			System.err.println("Erro Questao1 - teste04 - 1");
			return false;
		}

		if (resultado.length != 2) {
			System.err.println("Erro Questao1 - teste04 - 2");
			return false;
		}

		if (resultado[0] != 17) {
			System.err.println("Erro Questao1 - teste04 - 3");
			return false;
		}

		if (resultado[1] != 210) {
			System.err.println("Erro Questao1 - teste04 - 4");
			return false;
		}

		return true;
	}
	
	
	
	private static boolean teste05() {
		Questao1 questao = new Questao1();
		
		int[] resultado = questao.calcularSomaProduto(new int[]{ 10 });
		
		if (resultado == null) {
			System.err.println("Erro Questao1 - teste05 - 1");
			return false;
		}

		if (resultado.length != 2) {
			System.err.println("Erro Questao1 - teste05 - 2");
			return false;
		}

		if (resultado[0] != 10) {
			System.err.println("Erro Questao1 - teste05 - 3");
			return false;
		}

		if (resultado[1] != 10) {
			System.err.println("Erro Questao1 - teste05 - 4");
			return false;
		}

		return true;
	}


	
	private static boolean teste06() {
		Questao1 questao = new Questao1();
		
		int[] resultado = questao.calcularSomaProduto(new int[]{ 10, 20, 30, 0 });
		
		if (resultado == null) {
			System.err.println("Erro Questao1 - teste06 - 1");
			return false;
		}

		if (resultado.length != 2) {
			System.err.println("Erro Questao1 - teste06 - 2");
			return false;
		}

		if (resultado[0] != 60) {
			System.err.println("Erro Questao1 - teste06 - 3");
			return false;
		}

		if (resultado[1] != 0) {
			System.err.println("Erro Questao1 - teste06 - 4");
			return false;
		}

		return true;
	}


	
	private static boolean teste07() {
		Questao1 questao = new Questao1();
		
		int[] resultado = questao.calcularSomaProduto(new int[]{ 5, 3, -1 });
		
		if (resultado == null) {
			System.err.println("Erro Questao1 - teste07 - 1");
			return false;
		}

		if (resultado.length != 2) {
			System.err.println("Erro Questao1 - teste07 - 2");
			return false;
		}

		if (resultado[0] != 7) {
			System.err.println("Erro Questao1 - teste07 - 3");
			return false;
		}

		if (resultado[1] != -15) {
			System.err.println("Erro Questao1 - teste07 - 4");
			return false;
		}

		return true;
	}

	
	
	private static boolean teste08() {
		Questao1 questao = new Questao1();
		
		int[] resultado = questao.calcularSomaProduto(new int[]{ -1, -1, -1, -1, -1 });
		
		if (resultado == null) {
			System.err.println("Erro Questao1 - teste08 - 1");
			return false;
		}

		if (resultado.length != 2) {
			System.err.println("Erro Questao1 - teste08 - 2");
			return false;
		}

		if (resultado[0] != -5) {
			System.err.println("Erro Questao1 - teste08 - 3");
			return false;
		}

		if (resultado[1] != -1) {
			System.err.println("Erro Questao1 - teste08 - 4");
			return false;
		}

		return true;
	}

	
	
	private static boolean teste09() {
		Questao1 questao = new Questao1();
		
		int[] resultado = questao.calcularSomaProduto(new int[]{ -9, -9 });
		
		if (resultado == null) {
			System.err.println("Erro Questao1 - teste09 - 1");
			return false;
		}

		if (resultado.length != 2) {
			System.err.println("Erro Questao1 - teste09 - 2");
			return false;
		}

		if (resultado[0] != -18) {
			System.err.println("Erro Questao1 - teste09 - 3");
			return false;
		}

		if (resultado[1] != 81) {
			System.err.println("Erro Questao1 - teste09 - 4");
			return false;
		}

		return true;
	}

}
