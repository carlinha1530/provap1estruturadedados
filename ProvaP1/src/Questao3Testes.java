/**
 * 
 * Testes para a questao 3.
 * 
 * NAO ALTERE ESTE ARQUIVO.
 *
 */
public class Questao3Testes {

	public static void todosTestes() {
		
		boolean teste01 = teste01();
		
		boolean teste02 = teste02();
		
		boolean teste03 = teste03();
		
		boolean teste04 = teste04();
		
		boolean teste05 = teste05();
		
		boolean teste06 = teste06();
		
		boolean teste07 = teste07();
		
		boolean teste08 = teste08();
						
		if (
				teste01 &
				teste02 &
				teste03 &
				teste04 & 
				teste05 &
				teste06 & 
				teste07 &
				teste08 & 
				true) {
			System.out.println("Todos os testes para a Questao 3 realizados com sucesso!");
		}
	}

	private static boolean teste01() {
		Questao3 lista = new Questao3();
		
		lista.inserir(new Pessoa("Joao", 25));
		
		lista.inserir(new Pessoa("Maria", 10));
		
		lista.inserir(new Pessoa("Ruy", 50));
		
		lista.inserir(new Pessoa("Pedro", 66));
		
		int resultado = lista.contarMaisVelhos(20);
		
		if (resultado != 3) {
			System.err.println("Erro Questao3 - teste01 - 1");
			return false;
		}

		return true;
	}

	
	
	private static boolean teste02() {
		Questao3 lista = new Questao3();
		
		lista.inserir(new Pessoa("Joao", 25));
		
		lista.inserir(new Pessoa("Maria", 10));
		
		lista.inserir(new Pessoa("Ruy", 16));
		
		lista.inserir(new Pessoa("Pedro", 66));
		
		int resultado = lista.contarMaisVelhos(20);
		
		if (resultado != 2) {
			System.err.println("Erro Questao3 - teste02 - 1");
			return false;
		}

		return true;
	}

	
	private static boolean teste03() {
		Questao3 lista = new Questao3();
		
		lista.inserir(new Pessoa("Joao", 25));
		
		lista.inserir(new Pessoa("Maria", 10));
		
		lista.inserir(new Pessoa("Ruy", 50));
		
		int resultado = lista.contarMaisVelhos(20);
		
		if (resultado != 2) {
			System.err.println("Erro Questao3 - teste03 - 1");
			return false;
		}

		return true;
	}


	
	private static boolean teste04() {
		Questao3 lista = new Questao3();
		
		lista.inserir(new Pessoa("Joao", 25));
		
		lista.inserir(new Pessoa("Maria", 10));
		
		lista.inserir(new Pessoa("Ruy", 50));
		
		int resultado = lista.contarMaisVelhos(80);
		
		if (resultado != 0) {
			System.err.println("Erro Questao3 - teste04 - 1");
			return false;
		}

		return true;
	}
	
	
	
	private static boolean teste05() {
		Questao3 lista = new Questao3();
		
		int resultado = lista.contarMaisVelhos(20);
		
		if (resultado != 0) {
			System.err.println("Erro Questao3 - teste05 - 1");
			return false;
		}

		return true;
	}
	
	
	private static boolean teste06() {
		Questao3 lista = new Questao3();
		
		lista.inserir(new Pessoa("Joao", 25));
		
		lista.inserir(new Pessoa("Maria", 10));
		
		lista.inserir(new Pessoa("Ruy", 50));
		
		int resultado = lista.contarMaisVelhos(20);
		
		if (resultado != 2) {
			System.err.println("Erro Questao3 - teste06 - 1");
			return false;
		}

		return true;
	}
	
	
	
	private static boolean teste07() {
		Questao3 lista = new Questao3();
		
		lista.inserir(new Pessoa("Joao", 25));
		
		lista.inserir(new Pessoa("Maria", 10));
		
		lista.inserir(new Pessoa("Ruy", 50));
		
		int resultado = lista.contarMaisVelhos(5);
		
		if (resultado != 3) {
			System.err.println("Erro Questao3 - teste07 - 1");
			return false;
		}

		return true;
	}
	
	
	
	private static boolean teste08() {
		Questao3 lista = new Questao3();
		
		lista.inserir(new Pessoa("Joao", 25));
		
		lista.inserir(new Pessoa("Maria", 10));
		
		lista.inserir(new Pessoa("Ruy", 50));
		
		int resultado = lista.contarMaisVelhos(10);
		
		if (resultado != 2) {
			System.err.println("Erro Questao3 - teste08 - 1");
			return false;
		}

		return true;
	}
}
