/**
 * 
 * Questao sobre lista dinamica (lista ligada) desordenada.
 *
 */
public class Questao3 {

	No inicio = null;
	int qtdePessoas = 0;
	
	void inserir(Pessoa p) {
		No novo = new No();
		novo.info = p;
		
		novo.proximo = inicio;
		inicio = novo;
		
		++qtdePessoas;
	}
	/**
	 * Valor da questao: 5,0
	 * 
	 * Considerando a lista dinamica desordenada que armazena registros 
	 * do tipo Pessoa, retorne a quantidade de pessoas da lista que tenham 
	 * idade maior que a idade de corte passada como parametro de entrada.
	 * 
	 * Exemplo 1:
	 * inicio = {Joao|25, Maria|10, Ruy|50, Pedro|66}
	 * idadeCorte = 20
	 * retorno = 3
	 * 
	 * @param idadeCorte
	 * @return
	 * 
	 */
	int contarMaisVelhos(int idadeCorte) {
		int qtdeMaisVelhos = 0;
		No percorre = inicio;
		while(percorre != null){
			if(percorre.info.idade > idadeCorte);
			qtdeMaisVelhos++;
		}
		return qtdeMaisVelhos;
	}
}
